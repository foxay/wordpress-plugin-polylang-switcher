=== Polylang Language Switcher ===
Contributors: barto
Requires at least: 4.1
Tested up to: 6.0
Requires PHP: 5.4
Stable tag: 1.0.0
License: GPLv2 or later

== Description ==

**Shortcodes**

Language switcher for Polylang: polylang_switcher

Parameters:

* show_flags in logic 1 or 0 (default is "1")
* show_names in logic 1 or 0 (default is "1")

Returns:

`<ul class="polylang_langswitcher"> ... </ul>`

with the `polylang_langswitcher` for customization.

**Customization**

Sample CSS code for horizontal list of switches

```
.polylang_langswitcher > li{
	list-style: none;
	display: inline-block;
	padding: 10px;
}
```

**Contributions:**

Would you like to contribute to Polylang Language Switcher? 
You are more than welcome to submit your requests on the [0xacab repo](https://0xacab.org/foxay/wordpress-plugin-polylang-switcher). 
Also, if you have any notes about the code, please open a ticket on this issue tracker.

== Installation ==

**Automatic Installation**

1. Install using the WordPress built-in Plugin installer > Add New
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Go to the plugin page (under Dashboard > Accessibility)
1. Enjoy!

**Manual Installation**

1. Extract the zip file and just drop the contents in the <code>wp-content/plugins/</code> directory of your WordPress installation
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Go to the plugin page (under Dashboard > Accessibility)
1. Enjoy!

== Changelog ==

= 1.0.0 - 2022-10-31 =
* Initial Public Release!
