<?php
/*
Plugin Name:  Polylang Language Switcher
Version:      1.0.0
Author:       Barto 
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  polylang_switcher
Domain Path:  /languages
*/

/**
 * /**
 * The [polylang_switcher] shortcode.
 * Params: show_flags in logic 1 or 0 (default is "1"), show_names in logic 1 or 0 (default is "1").
 *
 * @param array  $atts    Shortcode attributes. Default empty.
 * @param string $content Shortcode content. Default null.
 * @param string $tag     Shortcode tag (name). Default empty.
 * @return string Shortcode output.
 */
function polylang_langs_switcher($atts = [], $content = null, $tag = '') { 
    $atts = array_change_key_case( (array) $atts, CASE_LOWER );
	$widget_atts = shortcode_atts(
		array(
			'show_flags' => 1,
			'show_names' => 1,
		), $atts, $tag
	);

    $output = ''; 
    if ( function_exists( 'pll_the_languages' ) ) { 
        $args = [ 
            'show_flags' => $widget_atts['show_flags'], 
            'hide_if_empty' => 0, 
            'show_names' => $widget_atts['show_names'], 
            'echo' => 0, 
        ]; 
        $output = '<ul class="polylang_langswitcher">'.pll_the_languages( $args ). '</ul>'; 
    }
    return $output; 
}

add_shortcode( 'polylang_switcher', 'polylang_langs_switcher' );
